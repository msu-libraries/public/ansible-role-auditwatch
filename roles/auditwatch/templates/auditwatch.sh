#!/usr/bin/env bash
# Renee Margaret McConahy <nepeta@msu.edu>
# @(#) v1.0.0 2019-12-10
#<
# Search audit logs using a particular checkpoint and optionally email a report
# when new records are found.
#
# Usage: auditwatch -c <checkpoint_file> [-k <audit_key>] \\
#            [-s <email_subject>] [-e <email>]...
#        auditwatch -C <config_file>
#        auditwatch -h
#
# If <checkpoint_file> is not a full path, it will be saved as
# /var/lib/misc/auditwatch-<CHECKPOINT_FILE>.
#
# The configuration file <config_file> should have one line per switch in the
# following format:
#
#     <argument_letter>: <argument_value>
#
# A switch without an argument is written as:
#
#     <argument_letter>
#
# Example:
#
#     c: my_checkpoint
#     k: UNDER_SIEGE
#     e: nepeta@localhost
#
#     # Comments and blank lines are also permitted.
#
#     # This is legal but pointless.
#     h
#>

# /usr/include/sysexits.h
declare -r EX_OK=0 EX_USAGE=64 EX_NOINPUT=66 EX_CANTCREAT=73

declare tmp="" email_subject="Auditwatch on ${HOSTNAME}"
onexit() {
    if [[ -f $tmp ]]; then
        if [[ $(stat -c %s -- "$tmp") != 0 ]]; then
            mail -s "$email_subject" "${email_addrs[@]}" <"$tmp"
        fi

        rm -f -- "$tmp"
    fi
}

ausearch_filter() {
    grep -vxF '<no matches>' >&2
}

trap onexit EXIT
set -eu

declare key="" checkpoint="" email_addrs=()

while getopts k:c:e:s:C:h opt; do
    case $opt in
    k) key=$OPTARG ;;
    c) checkpoint=$OPTARG ;;
    e) email_addrs+=("$OPTARG") ;;
    s) email_subject=$OPTARG ;;
    C)
        [[ -r $OPTARG ]] || exit $EX_NOINPUT
        while read -r s; do
            [[ $s =~ ^[[:space:]]*(#.*)?$ ]] && continue
            [[ $s =~ ^[A-Za-z](: .*)?$ ]] || exit $EX_USAGE
            read -r k v <<<"$s"
            if [[ $k == *: ]]; then
                set -- "$@" "-${k%:}" "$v"
            else
                set -- "$@" "-$k"
            fi
        done <"$OPTARG"

        unset s k v
        ;;
    h|*)
        sed -ne '/^#</,/^#>/ { /^#\(<\|>\)/d; s/^# \?//; p; }' -- "$0"
        [[ $opt == "?" ]] && exit $EX_USAGE
        exit $EX_OK ;;
    esac
done
shift $((OPTIND - 1))

(($#)) && exit $EX_USAGE

[[ -z $checkpoint ]] && exit $EX_USAGE
[[ $checkpoint == */* ]] || checkpoint=/var/lib/misc/auditwatch-$checkpoint
if [[ -d $checkpoint || ! -d ${checkpoint%/*} ]]; then
    echo "Bad checkpoint path: ${checkpoint@Q}" >&2
    exit $EX_CANTCREAT
fi
ausearch_args=(--checkpoint "$checkpoint")
[[ -n $key ]] && ausearch_args+=(-k "$key")

if ((${#email_addrs[@]})); then
    tmp=$(mktemp)
    exec &>"$tmp"
fi

ausearch "${ausearch_args[@]}" -i 2> >(ausearch_filter) &&:
r=$?
if [[ $r -eq 10 || $r -eq 11 || $r -eq 12 ]]; then
    ausearch "${ausearch_args[@]}" -ts checkpoint -i 2> >(ausearch_filter)
fi

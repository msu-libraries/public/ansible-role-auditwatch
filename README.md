# auditwatch: Install auditd rules and receive emails when things happen!

In addition to security auditing, the Linux auditing framework is useful for
troubleshooting: for example, it can record when processes modify particular
files or perform particular syscalls. This Ansible role adds the given rules to
a server and installs a script, run periodically by systemd, that sends email
notifications when any new events match. When you are finished, this role can
remove the rules, notification script, and associated configuration.

## Suggested usage
If you wanted to find out what was changing the ownership of `/usr` on
`machine.test`, you might make a playbook called `usr-problems.yml` that looked
like this:

```yml
- hosts: machine.test
  vars:
    auditwatch_watches:
      - rule: -a exit,always -F path=/usr -F perm=wa
        key: USR_PROBLEMS
        email_addresses:
          - you@workstation.test
        email_subject: "Something modified /usr on {{ inventory_hostname }}."
  roles:
    - auditwatch
```

To install it:

```sh
ansible-playbook usr-problems.playbook.yml
```

To remove everything installed by the role:

```sh
ansible-playbook usr-problems.playbook.yml -e auditwatch_mode=uninstall
```

To remove only the rules defined in a particular playbook, leaving other
auditwatch configurations intact:

```sh
ansible-playbook usr-problems.playbook.yml -e auditwatch_mode=remove
```

## Role variables
* `auditwatch_watches`: An array of configuration dictionaries; see the
  following.
* `auditwatch_watches[n].rule`: The text of the audit rule (see
  `audit.rules(7)`) to install.
* `auditwatch_watches[n].key`: The key to use for marking audit entries. This is
  capitalized for the log key and lowercased to produce the names of the
  auditwatch configuration and checkpoint files and systemd units.
* `auditwatch_watches[n].email_addresses`: An array of email addresses that
  should be notified of any matching log entries.
* `auditwatch_watches[n].email_subject`: Default: `Auditwatch on ${HOSTNAME}`.
* `auditwatch_mode`: One of `install` (default) to install auditwatch and all
  rules defined in `auditwatch_watches`, `remove` to remove rules defined in
  `auditwatch_watches`, or `uninstall` to remove rules and auditwatch. (`remove`
  would be useful when wishing to remove some rules while leaving others in
  place.)
* `auditwatch_run_interval`: Default: `*:0/10` (every ten minutes). See
  `systemd.time(7)`.
* `auditwatch_script_path`: The location of the script. (Default:
  `/usr/local/sbin/auditwatch`.)
* `auditwatch_config_dir`: Default: `/usr/local/etc/auditwatch`
* `auditwatch_auditd_config_file`: Default: `/etc/audit/audit.rules`

## F.A.Q.
* **Q.** Wouldn't it be far simpler to add the rule manually (`sed -i '$a -a
  exit,always -F path=/usr -F perm=wa -k USR_PROBLEMS' /etc/audit/audit.rules &&
  systemctl reload auditd`) and write a one-line cron job (`sed -i '$a */10 * *
  \* root ausearch --checkpoint /var/lib/misc/my.checkpoint |& grep -v "<no
  matches>" | mail -Es "It happened again." you@workstation.test'
  /etc/crontab`), making a note to remove the cron job, the audit rule, and
  check file when finished?

  **A.** Yes.
